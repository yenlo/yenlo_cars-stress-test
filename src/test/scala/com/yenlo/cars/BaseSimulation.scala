package com.yenlo.cars;

import io.gatling.core.Predef._
import io.gatling.http.Predef._

abstract class BaseSimulation extends Simulation {
  val marks = Array("A.C.","ALFA ROMEO","ALKO","ALPINE-RENAULT","ALVIS","AMERICAN MOTORS","AUDI","AUDI/PORSCHE","AUSTIN",
    "AUSTIN-HEALEY","AUTO BIANCHI","AUTO UNION","BAVARIA","BAVARIA CAMP","BECK","BENTLEY","BERTONE","BUERSTNER","BUICK OPEL",
    "CHALLENGER","CHRYSLER","CLASSIC ROADSTERS","CMS AUTO","COXX","D.K.W.","DACIA","DAIMLER BENZ","DATSUN","DERCKS","DETHLEFFS",
    "DIGICROSS","ESTEPE","EURA MOBIL","EUROPELIFT","EVOBUS","FIAT","FIAT CAM","FIAT-ABARTH","FIAT-FLEURETTE","FIAT-NECKAR",
    "FIAT/CHALLENGER","FORD","FRANKIA","FRANKIA-RMB","FSM","GALLOPER","HENDRICKS","HONDA","HYUNDAI","IFAKRAFTFARZEUGWERK AUDI",
    "ISUZU","IVECO","IVECO FIAT","KIA","KIA MOTORS","KIAMASTER","LATERAL DESIGN LIMITED","LECAPITAINE","MALLAGHAN ENGINEERING LTD",
    "MARUTI","MATTIG BAMBERG","MEIWA","MERCEDES","MERCEDES-AMG","MERCEDES-BENZ","MERCEDES-BENZ/SCHENK","MERCEDES-HYMER",
    "MERCEDES-RAPIDO","MITSUBISHI","MITSUBISHI FUSO","MORGAN","NEBIM","NECKAR","NILSSON","NSU-FIAT","OPEL","OPEL MOD DRST","POESSL",
    "POLSKI FIAT","PORSCHE","PORSCHE-DIESEL","QINGQUI","QUATTRO","RAMBLER RENAULT","RELIANT","RENAULT","RENAULT ALPINE",
    "RENAULT MOD DRST","RENAULT MOD JPM","RENAULT RODEO","RENAULT SAMSUNG","RENAULT SPORT","RENAULT TRUCKS","ROVER","SANTANA",
    "SAVIEM-RENAULT","SEAT","SIMCA-FIAT","SINGER","SKODA","SOVAB","STEYR-FIAT","STEYR-PUCH","STUDEBAKER","STX","SUZUKI","TEILHOL",
    "TEMPO","TERBERG TECHNIEK","TESLA","TESLA MOTORS","TOMOS","TOYOTA","TOYOTA-MINICRUISER","TSCHU-TSCHU","TVR","VAN HOOL-FIAT",
    "VOLKSWAGEN","VOLKSWAGEN-TABBERT","VOLKSWAGEN/DE VRIES","VOLVO","VOLVO-VERHEUL","VOLVO-ZABO","VOLVO/SCHENK","VW-PORSCHE",
    "WESTFIELD","WINNEBAGO","YAMAHA","YUGO","ZIE BIJZONDERHEDEN")
  val vrps = Array("00BBP2","00BBP9","00BBR1","ZB02VY","ZB02YX","ZB03BL","AE4781","AE4782","AE4794","1011EB","1016ZN","1017GB",
    "1022HG","3023US","3027XB","3029SB","3030BE","3033BB","3035BB","3037TF","3037UB","3039GX","3043MR","60AJ88","60AZ51","60BA11",
    "60BB42","60BBB1","60BBB3","60BBB7","60BBBB","60BBBD","60BBBJ","JB001K","JB001L","JB001P","JB001R","JB001V","JB001X","JB002B");
  val rand = new java.util.Random(System.currentTimeMillis());
  
  val count = http("count").get("/count");
  val count_marks = http("count-marks").get("/count/marks");
  val count_mark = http("count-mark").get("/count/mark/" + marks(rand.nextInt(marks.length)));
  val vrp = http("vrp").get("/vrp/" + vrps(rand.nextInt(vrps.length)));
  val vrp_notes_set = http("vrp-notes-set").put("/vrp/" + vrps(rand.nextInt(vrps.length)) + "/notes").
      body(StringBody("""{ "note": "Note about a car."}""")).asJSON;
  val vrp_notes_clear = http("vrp-notes-clear").put("/vrp/" + vrps(rand.nextInt(vrps.length)) + "/notes/clear");
}
