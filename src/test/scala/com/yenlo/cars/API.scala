package com.yenlo.cars;

import io.gatling.core.Predef._
import io.gatling.http.Predef._

abstract class API extends BaseSimulation {
  val headers = Map(
    "Content-Type"->"application/json",
    "Accept"->"application/json");
  
  val scn = scenario("cars").
    exec(count.basicAuth("yenlo-cars", "yenlo-cars-demo").headers(headers)).pause(0,3).
    exec(count_marks.basicAuth("yenlo-cars", "yenlo-cars-demo").headers(headers)).pause(0,5).
    exec(count_mark.basicAuth("yenlo-cars", "yenlo-cars-demo").headers(headers)).pause(0,5).
    exec(vrp.basicAuth("yenlo-cars", "yenlo-cars-demo").headers(headers)).
    exec(vrp_notes_set.basicAuth("yenlo-cars", "yenlo-cars-demo").headers(headers)).
    exec(vrp_notes_clear.basicAuth("yenlo-cars", "yenlo-cars-demo").headers(headers));
}
