package com.yenlo.cars;

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class LocalAPI extends API {
  val httpConf = http.baseURL("http://localhost:8080/cars");
  
  setUp(
    scn.inject(
      atOnceUsers(25), 
      rampUsers(25) over (30))
  ).protocols(httpConf);
}
