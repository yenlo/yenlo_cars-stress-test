package com.yenlo.cars;

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class HerokuAPI extends API {
  val httpConf = http.baseURL("https://yenlo-cars.herokuapp.com");
  
  setUp(
    scn.inject(
      atOnceUsers(25), 
      rampUsers(25) over (30))
  ).protocols(httpConf);
}
