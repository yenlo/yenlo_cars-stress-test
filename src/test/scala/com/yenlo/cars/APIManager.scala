package com.yenlo.cars;

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class APIManager extends BaseSimulation {
  val headers = Map(
    "Content-Type"->"application/json",
    "Accept"->"application/json",
    // change to valid token
    "Authorization"->"Bearer 427215dd-a899-3948-b619-fd396304439b");
  
  val scn = scenario("cars").
    exec(count.headers(headers)).pause(0,3).
    exec(count_marks.headers(headers)).pause(0,5).
    exec(count_mark.headers(headers)).pause(0,5).
    exec(vrp.headers(headers)).
    exec(vrp_notes_set.headers(headers)).
    exec(vrp_notes_clear.headers(headers));
    
  val httpConf = http.baseURL("http://localhost:8280/cars/0.0.1");
  setUp(
    scn.inject(
      atOnceUsers(25), 
      rampUsers(25) over (30))
  ).protocols(httpConf);
}
