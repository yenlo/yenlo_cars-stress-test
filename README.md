![Yenlo](http://mms.businesswire.com/media/20160329005629/en/516023/21/Yenlo_logo_square.jpg)

# Stress tests
The WSO2 products installations should end with tests which proof that system is working stable and fast. The tests can show that some settings are not correct or that installation can be tuned.
## Goal
Demonstration how to create tests for a published API in WSO2 API Manager. As a test environment used stress tool software [gatling.io](http://gatling.io).
## Requirements
* Install [WSO2 API Manager](http://wso2.com/products/api-manager) and start it.
* Publish a Yenlo Cars API which can be build and run on you environment (local computer) or using Heroku application servers.
  * Local
    * download project [Yenlo Cars API](https://bitbucket.org/yenlo/yenlo_cars)
    * upload schema and data to MySQL
    ```sh
    mysql> source yenlo-cars-import/sql/schema.sql;
    mysql> source yenlo-cars-import/sql/cars-backup.sql;
    ```
    * build project (Java8 required) under yenlo-cars directory
    ```sh
    $ mvn clean install
    ```
    * run server
    ```sh
    $ java -jar yenlo-cars-api/target/dependency/webapp-runner.jar --path /cars --port 8080 yenlo-cars-api/target/*.war
    ```
    * upload API to WSO2 API Manager USING the Swagger URL http://localhost:8080/cars/v2/api-docs
  * Heroku
    * upload API to WSO2 API Manager USING the Swagger URL https://yenlo-cars.herokuapp.com
  * In both cases set endpoint’s security as “Basic Auth” with credentials user as yenlo-cars, password as yenlo-cars-demo.  
* Subscribe to Yenlo Cars API

## Tests
### Scenario
Let’s take that we would like to test the action on listed URL’s:

* GET /count
* GET /count/marks
* GET /count/mark/{mark}
* GET /vrp/{vehicleRegistrationPlate}
* PUT /vrp/{vehicleRegistrationPlate}/notes
* PUT /vrp/{vehicleRegistrationPlate}/notes/clear

Created 25 users at one time and 25 users during 30 seconds which will request listed URLs.
### Run tests
```sh
# run tests on WSO2 API Manager
$ mvn gatling:execute -Dgatling.simulationClass=com.yenlo.cars.APIManager
# run tests on local machine to compare with WSO2 API Manager
$ mvn gatling:execute -Dgatling.simulationClass=com.yenlo.cars.LocalAPI
# run tests on Heroku to compare with WSO2 API Manager
$ mvn gatling:execute -Dgatling.simulationClass=com.yenlo.cars.HerokuAPI
```